var server     = require('http').createServer(),
    io         = require('socket.io')(server),
    port       = 3000;


io.on('connection', function (socket){

    socket.on('create_order', function (data) {

        io.emit('replace_order');
        io.emit('replace_unread_counter');
        console.log('create order #'+ data +' ' + Date());

    });

    socket.on('check_as_read', function() {
        io.emit('replace_order');
        io.emit('replace_unread_counter');
        io.emit('replace_read_counter');
    });

    socket.on('update_order',  function (data) {
        io.emit('replace_order');
        console.log('update order #'+ data+' ' + Date());
    });

    socket.on('cansel_order',  function (data) {
        io.emit('replace_order');
        console.log('cansel order #'+ data+' ' + Date());
    });

    socket.on('delete_order', function (data) {
        io.emit('replace_order');
        io.emit('replace_read_counter');
        io.emit('replace_unread_counter');
        console.log('delete order #'+ data+' ' + Date());
    });
});


server.listen(port, function() {

    console.log('Server listening at', port);
});