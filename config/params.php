<?php

return [
    'adminEmail' => 'admin@taxicomfort.by',
    'supportEmail' => 'info@taxicomfort.by',
    'user.passwordResetTokenExpire' => 3600,
];
