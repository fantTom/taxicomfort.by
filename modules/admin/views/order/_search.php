<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderSearh */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'customername') ?>

    <?= $form->field($model, 'phonenumber') ?>

    <?= $form->field($model, 'street') ?>

    <?= $form->field($model, 'housenumber') ?>

    <?php // echo $form->field($model, 'hullnumber') ?>

    <?php // echo $form->field($model, 'entrancenumber') ?>

    <?php // echo $form->field($model, 'commentcustomer') ?>

    <?php // echo $form->field($model, 'orderdate') ?>

    <div class="form-group">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Обновить', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
