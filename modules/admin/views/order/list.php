<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Лист заказов';
$this->params['breadcrumbs'][] = ['label' => 'Заказ', 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operator-order-list">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="order-content">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items} {summary} {pager}',
            'rowOptions' => function($model) {
                if (!$model->created_at) return ['class' => 'danger'];
            },
            'columns' => [
                //['class' => 'yii\grid\ActionColumn'],
                ['class' => 'yii\grid\SerialColumn'],
                //'id',
                'phonenumber',
                'customername',
                'street',
                'housenumber',
                //'hullnumber',
                'entrancenumber',
                'commentcustomer:ntext',
                'created_at:datetime',
                'status',

               /*[
                    'class' => 'yii\grid\ActionColumn',
                    'header' => '<center>Mark as Read</center>',
                    'template' => '{view}',
                    'visibleButtons' => [
                        'view' => function($model) {
                            return !$model->created_at;
                        }
                    ],
                    'buttons' => [
                        'view' => function($url, $model) {
                            $span = '<span class="glyphicon glyphicon-ok"></span>';
                            return Html::a($span, "/guestbook/check-as-read?id={$model->id}", [
                                'class' => 'view_message',
                                'value' => $model->id,
                                'title' => 'Mark as read'
                            ]);
                        }
                    ]
                ],*/
            ],
        ]) ?>

    </div>
</div>

<?php



$this->registerCss('
    .summary {
        display: inline-block;
    }
    .pagination {
        margin: 0px;
        float: right !important;
    }
    .order-content table > tbody > tr > td:last-of-type {
        text-align: center;
    }
');

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js');
$this->registerJs('
    const socket = io("http://localhost:3000")
    const url = window.location.origin

    function replaceListTable() {
        $.ajax({
            url: url + "/operator/order/list-partial",
            method: "get",
            success: function(data) {
                $(".order-content").replaceWith(data)
            }
        })
    }

    function replaceUnreadCounter() {
        $.ajax({
            url: url + "/operator/order/unread-counter",
            method: "get",
            success: function(data) {
                $(".unread-counter").text(data)
            }
        })
    }

    function replaceReadCounter() {
        $.ajax({
            url: url + "/operator/order/read-counter",
            method: "get",
            success: function(data) {
                $(".read-counter").text(data)
            }
        })
    }

    function checkAsRead(id, url) {
        $.ajax({
            url: url,
            method: "get",
            success: function(data) {}
        })
    }

    socket.on("replace_order", function(data) {
        replaceListTable()
    })

    socket.on("replace_unread_counter", function() {
        replaceUnreadCounter()
    })

    socket.on("replace_read_counter", function() {
        replaceReadCounter()
    })

    $(".order-list").on("click", ".view_message", function(e) {
        e.preventDefault()
        checkAsRead($(this).attr("value"), $(this).attr("href"))
    })
');
?>
