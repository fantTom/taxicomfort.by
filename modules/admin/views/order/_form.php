<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class ="form-group row">
        <div class="form-group col-md-4">
            <?= $form->field($model, 'phonenumber')->widget(MaskedInput::className(), [
                'mask' => '+375 (99) 999 99 99'
            ]); ?>
        </div>
        <div class="form-group col-md-8">
            <?= $form->field($model, 'customername')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

        <div class="form-group">
            <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
        </div>
    <div class="form-group">
        <br> <!--Для того, что бы одинаково смотрелось. Самый простой вариант. -->
    </div>
        <div class ="form-group row">
             <div class="form-group col-md-4">
                 <?= $form->field($model, 'housenumber')->textInput() ?>
             </div>
             <div class="form-group col-md-4">
                 <?= $form->field($model, 'hullnumber')->textInput(['maxlength' => true, 'value'=>'-'])?>
             </div>
             <div class="form-group col-md-4">
                 <?= $form->field($model, 'entrancenumber')->textInput(['maxlength' => true, 'value'=>'-']) ?>
             </div>
         </div>
        <div class="form-group">
            <?= $form->field($model, 'commentcustomer')->textarea(['rows' => 3]) ?>
        </div>
        <?//= $form->field($model, 'orderdate')->textInput() ?>

        <div class="form-group">

             <?= Html::submitButton('Продолжить', ['class' => 'btn btn-success'])?>
        </div>
     <?php ActiveForm::end(); ?>


</div>
