<?php

use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="order-content">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items} {summary} {pager}',
        'columns' => [
            //['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\SerialColumn'],
            'phonenumber',
            'customername',
            'street',
            'housenumber',

            'entrancenumber',
            'commentcustomer:ntext',
            'created_at:datetime',
           'status',


           /* [
                'class' => 'yii\grid\ActionColumn',
                'header' => '<center>Mark as Read</center>',
                'template' => '{view}',
                'visibleButtons' => [
                    'view' => function($model) {
                        return !$model->read;
                    }
                ],
                'buttons' => [
                    'view' => function($url, $model) {
                        $span = '<span class="glyphicon glyphicon-ok"></span>';
                        return Html::a($span, "/order/check-as-read?id={$model->id}", [
                            'class' => 'view_message',
                            'value' => $model->id,
                            'title' => 'Mark as read'
                        ]);
                    }
                ]
            ],*/
        ],
    ]); ?>
</div>