<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Order */

$this->title = $model->customername;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить заказ', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отменить заказ', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите отменить заказ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php Modal::begin([
        'header' => '<h2>Ваш заказ!</h2>',
        'clientOptions' => ['show' => true],
        /*'toggleButton' => [
        'label' => 'click me',
        'tag' => 'button',
        'class' => 'btn btn-success',
        ],*/
        'footer' => '',
    ]);?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //  'id',
            'customername',
            'phonenumber',
            'street',
            'housenumber',
            'hullnumber',
            'entrancenumber',
            'commentcustomer:ntext',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
    <?php Modal::end(); ?>

</div>
