<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \app\modules\user\models\User */

$this->title = 'Админ панель';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-default-index">

    <h1><?//= Html::encode($this->title) ?></h1>

    <p>
        <?//= Html::a('Пользователи', ['users/index'], ['class' => 'btn btn-primary']) ?>
    </p>
</div>
