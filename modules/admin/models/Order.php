<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $customername
 * @property string $phonenumber
 * @property string $street
 * @property int $housenumber
 * @property string $hullnumber
 * @property string $entrancenumber
 * @property string $commentcustomer
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Order extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    const STATUS_CANCEL = 0;
    const STATUS_WAIT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_READY = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    public function getStatusName()
    {
        return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_CANCEL => 'Отменена',
            self::STATUS_WAIT => 'Новая',
            self::STATUS_ACTIVE => 'Обработана',
            self::STATUS_READY=> 'Выполнена',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customername' => 'Имя',
            'phonenumber' => 'Номер телефона',
            'street' =>  'Улица',
            'housenumber' => 'Дом',
            'hullnumber' => 'Корпус',
            'entrancenumber' => 'Подъезд',
            'commentcustomer' => 'Комментарий',
            'status' => 'Статус',
            'created_at' =>  'Дата заказа',
            'updated_at' =>  'Дата изменения',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phonenumber', 'customername', 'street', 'housenumber'], 'required'],
            [['housenumber'], 'integer'],
            [['commentcustomer','phonenumber'], 'string'],
            ['phonenumber', 'match', 'pattern' => '/^(\+375\s[(](33|44|29|25)[)]\s([1-9]{3})\s(\d{2})\s(\d{2}))$/', 'message' => 'Не соответствует'],
            [['customername', 'street', 'hullnumber', 'entrancenumber'], 'string', 'max' => 255],
            [['hullnumber', 'entrancenumber'], 'string', 'max' => 5],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        return;
    }
}
