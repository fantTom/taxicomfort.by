<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrderSearh represents the model behind the search form of `app\modules\admin\models\Order`.
 */
class OrderSearh extends Order
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'phonenumber', 'housenumber'], 'integer'],
            [['customername', 'street', 'hullnumber', 'entrancenumber', 'commentcustomer', 'created_at'], 'safe'],
            [['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'phonenumber' => $this->phonenumber,
            'housenumber' => $this->housenumber,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'customername', $this->customername])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'hullnumber', $this->hullnumber])
            ->andFilterWhere(['like', 'entrancenumber', $this->entrancenumber])
            ->andFilterWhere(['like', 'commentcustomer', $this->commentcustomer])
            ->andFilterWhere(['>=', 'created_at', $this->date_from ? strtotime($this->date_from . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'created_at', $this->date_to ? strtotime($this->date_to . ' 23:59:59') : null]);

        return $dataProvider;
    }
}
