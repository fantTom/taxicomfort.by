<?php

namespace app\modules\user\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property int $author_id
 * @property int $assessment
 * @property string $negative
 * @property string $positive
 * @property int $created_at
 * @property int $updated_at
 */
class Reviews extends ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_id', 'positive'], 'required'],
            [['author_id', 'assessment'], 'integer'],
            [['negative', 'positive'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Автор отзыва',
            'assessment' => 'Рейтинг',
            'negative' => 'Недостатки',
            'positive' => 'Достоинста',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    public function getAuthor(){
        return $this->hasOne(User::className(),['id' => 'author_id' ]);
    }

}
