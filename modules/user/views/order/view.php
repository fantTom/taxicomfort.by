<?php

use app\modules\admin\models\Order;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Order */

$this->title = $model->customername;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) . ' ' .$model->phonenumber?> </h1>
    <p>
        <?=$model->street . ', дом ' . $model->housenumber . ', корпус ' . $model->hullnumber . ', квартира ' .$model->entrancenumber?>
    </p>

    <p>
        <?= Html::a('Изменить заказ', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Отменить заказ', ['cancel', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите отменить заказ?',
                'method' => 'post',
            ],
        ]) ?>

    </p>


    <?php Modal::begin([
        'header' => '<h2>Ваш заказ!</h2>',
        'clientOptions' => ['show' => true],
        'footer' => '',
    ]);?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //  'id',
            'customername',
            'phonenumber',
            'street',
            'housenumber',
            'hullnumber',
            'entrancenumber',
            'commentcustomer:ntext',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
    <?php Modal::end(); ?>

</div>
