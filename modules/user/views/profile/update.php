<?php
/**
 * Created by PhpStorm.
 * User: FantTom
 * Date: 07.01.2019
 * Time: 5:03
 */
$this->title = 'Редактировать';
$this->params['breadcrumbs'][] = ['label' => 'Профиль', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="user-profile-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="user-form">
         <?php $form = ActiveForm::begin([]); ?>

             <?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
