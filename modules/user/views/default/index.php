<div class="user-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        Это просмотр содержимого для действий "<?= $this->context->action->id ?>".
        Действие принадлежит контроллеру "<?= get_class($this->context) ?>"
         в "<?= $this->context->module->id ?>" модуле.
    </p>
    <p>
        Вы можете настроить эту страницу, отредактировав следующий файл:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div>
