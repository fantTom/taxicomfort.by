<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\Reviews */

$this->title = 'Добавление отзыва';
$this->params['breadcrumbs'][] = ['label' => 'Отзывы', 'url' => ['/main/reviews/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reviews-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
