<?php

use kartik\rating\StarRating;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\Reviews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reviews-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'author_id')->textInput() ?>

    <?= $form->field($model, 'assessment')->widget(StarRating::classname(), [
            'name' => 'rating_33',
            'pluginOptions' => ['step' =>1]
]); ?>

    <?= $form->field($model, 'negative')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'positive')->textarea(['rows' => 3]) ?>

    <?//= $form->field($model, 'created_at')->textInput() ?>

    <?//= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
