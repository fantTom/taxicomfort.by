<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.01.2019
 * Time: 14:37
 */

namespace app\modules\user\controllers;


use Yii;
use app\modules\admin\models\Order;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use ElephantIO\Client as Socket;
use ElephantIO\Engine\SocketIO\Version1X;
use ElephantIO\Engine\SocketIO\Version2X;

require __DIR__ . '/../../../vendor/autoload.php';

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    protected $socket;
    protected $pageSize;

    /**
     * Inisialisasi awal sebelum method dieksekusi.
     * @return void
     */
    public function init()
    {
        $this->socket = new Socket(new Version2X('http://localhost:3000'));
        $this->pageSize = 15;
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post() )&& $model->status = '1' ){
                if( $model->save()) {

                $this->socket->initialize();
                $this->socket->emit('create_order', [$model->id]);
                $this->socket->close();

                Yii::$app->session->setFlash('success',  $model->customername . ' cпасибо за Ваш заказ!');
                return $this->redirect(['view', 'id' => $model->id]);

            } else {
                Yii::$app->session->setFlash('error', 'Что-то пошло не так....');

        }}
        return $this->render('create', [
            'model' => $model
        ]);
    }
    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->status = '1';

        if ($model->load(Yii::$app->request->post() )&&  $model->save()) {
                $this->socket->initialize();
                $this->socket->emit('update_order', [$id]);
                $this->socket->close();
                Yii::$app->session->setFlash('success', 'Ваша заявка отменена');
                return $this->redirect(['view', 'id' => $model->id]);
            }else{

                return $this->render('update', [
                    'model' => $model,
                ]);
    }
    }

    /**
     * Calsels an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCancel($id)
    {
        $model = $this->findModel($id);
        $model->status = '0';

        if ($model->load(Yii::$app->request->post() )&&  $model->save()) {
                $this->socket->initialize();
                $this->socket->emit('cansel_order', [$id]);
                $this->socket->close();

                Yii::$app->session->setFlash('success', 'Ваша заявка отменена');
                return $this->render('update', [
                    'model' => $model,
                ]);
            } else{
                Yii::$app->session->setFlash('error', 'Что-то пошло не так....');

                return $this->render('view', ['model' => $model]);
            }
    }


    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('Запрашиваемая страница не существует.');

        }
    }
}