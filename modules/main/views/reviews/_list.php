<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17.01.2019
 * Time: 9:06
 */

use kartik\rating\StarRating; ?>

<div class="reviews-list">

    <div style="border-style:outset; border-radius: 50px; padding:0px 40px 5px 40px; margin: 5px;">

                <h3 class="card-title">
                    <?= StarRating::widget([
                'name' => 'rating_21',
                'value' => $model->assessment,
                'pluginOptions' => [
                    'readonly' => true,
                    'showClear' => false,
                    'showCaption' => false,
                ],
            ]);
            ?> </h3>
                <p class="card-text"> Недостатки:  <?= $model->negative ?> </p>
                <p class="card-text"> Достоинства: <?= $model->positive ?> </p>

        <p style="align-text: center">Считает <?=$model->author->username?>  в <?=Yii::$app->formatter->asDatetime($model->created_at, "php:H:s d-m-Y ")?> </p>

    </div>
</div>
