<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 17.01.2019
 * Time: 9:06
 */

?>

<div class="news-list">

    <div style="border-style:outset; border-radius: 50px; padding:0px 30px 30px 30px; margin: 20px;">

                <h3 class="card-title"><?=$model->title ?></h3>
                <p class="card-text"> <?= $model->text ?> </p>
        <hr>
                <?=Yii::$app->formatter->asDateTime($model->created_at, 'php:H:s d-m-Y')?>

    </div>
</div>
