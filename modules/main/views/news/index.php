<?php

use yii\bootstrap\Button;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\operator\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-news-index">

    <h1><?= Html::encode($this->title) ?></h1>

  <?=  Yii::$app->user->can('operator') ?
      Html::a('Добавить новость',['/operator/news/create'],['class'=>'btn btn-primary']):
       false
?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
       'itemView' => function ($model, $key, $index, $widget) {
        return $this->render('_list',['model' => $model]);
           // or just do some echo
           // return $model->title . ' posted by ' . $model->author;
       },
       'options' => [
            'tag' => 'div',
            'class' => 'list-wrapper',
            'id' => 'list-wrapper',
        ],

        'summary' => 'Показано {count} из {totalCount}',
        'layout' => "{pager}\n{summary}\n{items}\n{pager}",
         'summaryOptions' => [
            'tag' => 'span',
            'class' => 'my-summary'
        ],
/*
        'itemOptions' => [
            'tag' => false,
        ],*/
        'pager' => [
            'firstPageLabel' => 'Первая',
            'lastPageLabel' => 'Последняя',
            'nextPageLabel' => 'Следующая',
            'prevPageLabel' => 'Предыдущая',
            'maxButtonCount' => 5,
        ],

    ]); ?>
</div>
