<?php

use app\modules\admin\models\Order;
use kartik\date\DatePicker;
use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Лист заявок';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-list">


    <?php Pjax::begin(); ?>
    <?= Html::beginForm();?>
  <?=  DatePicker::widget([
    'name' => 'from_date',
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    //'value' => date('Y-m-d'),
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
        ]
    ]);?>
    <?=  Html::submitButton('Показать');?>
    <?=  Html::endForm();?>

    <button class="btn btn-primary" type="button">
        всего <span class="badge read-counter"><?= $todayMessages ? $todayMessages : 0 ?></span> заявок
    </button>

    <button class="btn btn-danger" type="button">
        Новых  <span class="badge read-counter"><?= $readMessages ? $readMessages : 0 ?></span> заявок
    </button>

    <button class="btn btn-success" type="button">
        Обработанных <span class="badge read-counter"><?= $processedMessages ? $processedMessages : 0 ?></span> заявок
    </button>

    <button class="btn btn-warning" type="button">
        Отмененых <span class="badge unread-counter"><?= $canceledMessages ? $canceledMessages : 0 ?></span> заявок
    </button>

    <button class="btn btn-info" type="button">
        Выполненых <span class="badge unread-counter"><?= $unreadMessages ? $unreadMessages : 0 ?></span> заявок
    </button>

    <h1><?//= Html::encode($this->title) ?></h1>

    <div class="order-content">

        <?php $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'phonenumber',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    /** @var Order $model */
                    return Html::a(Html::encode($model->phonenumber), ['view', 'id' => $model->id]);
                }
            ],
            'customername',
            'street',
            'housenumber',
            'entrancenumber',

            'commentcustomer:ntext',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:H:i:s d-m-Y'],

                'contentOptions' => function($model){
                    return [
                        'class' => (empty($model->created_at)) ? 'empty-cell' : ''
                    ];
                }
            ],
            [
                'filter' => Order::getStatusesArray(),
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    /** @var Order $model */
                    /** @var \yii\grid\DataColumn $column */
                    $value = $model->{$column->attribute};
                    switch ($value) {
                        case Order::STATUS_ACTIVE:
                            $class = 'success';
                            break;
                        case Order::STATUS_CANCEL:
                            $class = 'warning';
                            break;
                        case Order::STATUS_WAIT:
                            $class = 'danger';
                            break;
                        case Order::STATUS_READY:
                            $class = 'info';
                            break;
                        default:
                            $class = 'default';
                    };
                    $html = Html::tag('span', Html::encode($model->getStatusName()), ['class' => 'label label-' . $class]);
                    return $value === null ? $column->grid->emptyCell : $html;
                }
            ]

        ] ?>

        <?= ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
        ]);?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterSelector' => 'filter-form',
            'layout' => '{items} {summary} {pager}',

            'rowOptions' => function($model) {
                if (!$model->created_at) return ['class' => 'danger'];
            },
            'columns' => $gridColumns
        ]) ?>

    </div>
    <?php Pjax::end(); ?>

</div>

<?php

$this->registerCss('
    .summary {
        display: inline-block;
    }
    .pagination {
        margin: 0px;
        float: right !important;
    }
    .order-content table > tbody > tr > td:last-of-type {
        text-align: center;
    }
');

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js');
$this->registerJs('
    const socket = io("http://localhost:3000")
    const url = window.location.origin

    function replaceListTable() {
        $.ajax({
            url: url + "/operator/order/list-partial",
            method: "get",
            success: function(data) {
                $(".order-content").replaceWith(data)
            }
        })
    }

    function replaceUnreadCounter() {
        $.ajax({
            url: url + "/operator/order/unread-counter",
            method: "get",
            success: function(data) {
                $(".unread-counter").text(data)
            }
        })
    }

    function replaceReadCounter() {
        $.ajax({
            url: url + "/operator/order/read-counter",
            method: "get",
            success: function(data) {
                $(".read-counter").text(data)
            }
        })
    }

    function checkAsRead(id, url) {
        $.ajax({
            url: url,
            method: "get",
            success: function(data) {}
        })
    }

    socket.on("replace_order", function(data) {
        replaceListTable()
    })

    socket.on("replace_unread_counter", function() {
        replaceUnreadCounter()
    })

    socket.on("replace_read_counter", function() {
        replaceReadCounter()
    })
    
');


?>
