<?php

use app\modules\admin\models\Order;
use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="order-content">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '{items} {summary} {pager}',
        'rowOptions' => function($model) {
            if (!$model->created_at) return ['class' => 'danger'];
        },
        'columns' => [
            //['class' => 'yii\grid\ActionColumn'],
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'phonenumber',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    /** @var Order $model */
                    return Html::a(Html::encode($model->phonenumber), ['view', 'id' => $model->id]);
                }
            ],
            'customername',
            'street',
            'housenumber',
            //'hullnumber',
            'entrancenumber',
            'commentcustomer:ntext',
            'created_at:datetime',
            [
                'filter' => Order::getStatusesArray(),
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    /** @var Order $model */
                    /** @var \yii\grid\DataColumn $column */
                    $value = $model->{$column->attribute};
                    switch ($value) {
                        case Order::STATUS_ACTIVE:
                            $class = 'success';
                            break;
                        case Order::STATUS_CANCEL:
                            $class = 'warning';
                            break;
                        case Order::STATUS_WAIT:
                            $class = 'danger';
                            break;
                        case Order::STATUS_READY:
                            $class = 'info';
                            break;
                        default:
                            $class = 'default';
                    };
                    $html = Html::tag('span', Html::encode($model->getStatusName()), ['class' => 'label label-' . $class]);
                    return $value === null ? $column->grid->emptyCell : $html;
                }
            ],

        ],
    ]) ?>
</div>