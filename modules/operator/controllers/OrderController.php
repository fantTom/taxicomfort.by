<?php
/**
 * Created by PhpStorm.
 * User: FantTom
 * Date: 07.01.2019
 * Time: 15:55
 */

namespace app\modules\operator\controllers;

use app\modules\admin\models\Order;
use app\modules\admin\models\OrderSearh;
use ElephantIO\Client as Socket;
use ElephantIO\Engine\SocketIO\Version2X;
use Yii;
use yii\base\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class OrderController extends Controller
{
    protected $socket;
    protected $pageSize;

    /**
     * Inisialisasi awal sebelum method dieksekusi.
     * @return void
     */
    public function init()
    {
        $this->socket = new Socket(new Version2X('http://localhost:3000'));
        $this->pageSize = 15;
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Menampilkan data order.
     * @return mixed Render view dan variabel yang didefinisikan.
     */
    public function actionList()
    {
        $fromDate = Yii::$app->request->post('from_date');

        $todayMessages = Order::find()
            ->filterWhere(['>=', 'created_at', $fromDate ? strtotime($fromDate . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'created_at', $fromDate ? strtotime($fromDate . ' 23:59:59') : null])->count();
        $readMessages =  Order::find()->where(['status' => 1])
            ->andfilterWhere(['>=', 'created_at', $fromDate ? strtotime($fromDate . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'created_at', $fromDate ? strtotime($fromDate . ' 23:59:59') : null])->count();
        $processedMessages=Order::find()->where(['status' => 2])
            ->andfilterWhere(['>=', 'created_at', $fromDate ? strtotime($fromDate . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'created_at', $fromDate ? strtotime($fromDate . ' 23:59:59') : null])->count();
        $canceledMessages = Order::find()->where(['status' => 0])
            ->andfilterWhere(['>=', 'created_at', $fromDate ? strtotime($fromDate . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'created_at', $fromDate ? strtotime($fromDate . ' 23:59:59') : null])->count();
        $unreadMessages = Order::find()->where(['status' => 3])
            ->andfilterWhere(['>=', 'created_at', $fromDate ? strtotime($fromDate . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'created_at', $fromDate ? strtotime($fromDate . ' 23:59:59') : null])->count();


        $dataProvider = new ActiveDataProvider([
            'query' => Order::find()
                ->filterWhere(['>=', 'created_at', $fromDate ? strtotime($fromDate . ' 00:00:00') : null])
                ->andFilterWhere(['<=', 'created_at', $fromDate ? strtotime($fromDate . ' 23:59:59') : null])
                ->orderBy([
                    'created_at' => SORT_DESC
                ]),
            'pagination' => [
                'pageSize' => $this->pageSize
            ]
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'todayMessages' =>$todayMessages,
            'readMessages' => $readMessages,
            'processedMessages' => $processedMessages,
            'canceledMessages' => $canceledMessages,
            'unreadMessages' => $unreadMessages,
        ]);
    }

    /**
     * Menampilkan data guestbook yang di request menggunakan ajax dari view list.php
     * @return string Parsing data html hasil dari render partial.
     */
    public function actionListPartial()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Order::find()->where('DATE(created_at)=CURDATE()')->orderBy([
                'created_at' => SORT_DESC
            ]),
            'pagination' => [
                'pageSize' => $this->pageSize
            ]
        ]);
       Yii::$app->session->setFlash('success', '111');
        $pagination = $dataProvider->getPagination();
        $pagination->route = '/operator/order/list';

        return $this->renderPartial('list_partial', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param $id
     * @return Order|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {

            throw new NotFoundHttpException('Запрашиваемая страница не существует.');

        }
    }


}