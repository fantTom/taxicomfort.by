<?php

namespace app\modules\operator\models;

use app\modules\user\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property int $author_id
 * @property int $category_id
 * @property string $text
 * @property int $created_at
 * @property int $updated_at
 */
class News extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [

            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['title'], 'string', 'max' => 55],
            [['text'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
           // 'id' => 'ID',
            'title' => 'Заголовок',
           'author_id' => 'Автор',
            'category_id' => 'Выбор категории',
            'text' => 'Текст',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изминения',
        ];
    }
    public function getAuthor(){
        return $this->hasOne(User::className(),['id' => 'author_id' ]);
    }
}
