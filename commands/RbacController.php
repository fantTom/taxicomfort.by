<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 */
class RbacController extends Controller
{

    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        // Создадим роли админа, водителя, диспетчера, пользователя
        $admin = $auth->createRole('admin');
        $operator = $auth->createRole('operator');
        $driver = $auth->createRole('driver');

        $user = $auth->createRole('user');

        // запишем их в БД
        $auth->add($admin);
        $auth->add($driver);
        $auth->add($operator);
        $auth->add($user);

        $viewAdminPage = $auth->createPermission('viewAdminPanel');
        $viewAdminPage->description = 'Просмотр админки';

        $createNews = $auth->createPermission('createNews');
        $createNews->description = 'Создание новости';

        $updateNews = $auth->createPermission('updateNews');
        $updateNews->description = 'Редактирование новости';

        $viewOrders= $auth->createPermission('viewOrder');
        $viewOrders->description = 'Просмотр заказa';

        $updateOrder = $auth->createPermission('updateOrder');
        $updateOrder->description = 'Редактирование заказa';

        // Запись разрешений в БД
        $auth->add($viewAdminPage);
        $auth->add($createNews);
        $auth->add($updateNews);
        $auth->add($viewOrders);
        $auth->add($updateOrder);

        $auth->addChild($operator,$updateNews);
        $auth->addChild($operator,$viewOrders);
        $auth->addChild($operator,$updateOrder);
        $auth->addChild($operator,$createNews);

        $auth->addChild($admin, $viewAdminPage);
        $auth->addChild($admin, $operator);


        // Назначаем роль admin пользователю с ID 1
        $auth->assign($admin, 1);
        $auth->assign($operator, 2);
        $auth->assign($driver, 3);
    }
}