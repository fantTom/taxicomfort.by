<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bodytypes`.
 */
class m190106_185103_create_bodytypes_table extends Migration
{
    /**
     * тип кузова
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bodytypes', [
            'id' => $this->primaryKey(),
            'nametype' => $this->string()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bodytypes');
    }
}
