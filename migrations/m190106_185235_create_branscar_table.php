<?php

use yii\db\Migration;

/**
 * Handles the creation of table `branscar`.
 */
class m190106_185235_create_branscar_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('branscar', [
            'id' => $this->primaryKey(),
            'namebrand' =>$this->string()->unique(),
            'country' => $this->string()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('branscar');
    }
}
