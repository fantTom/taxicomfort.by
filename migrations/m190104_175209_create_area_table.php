<?php

use yii\db\Migration;

/**
 * Handles the creation of table `area`.
 */
class m190104_175209_create_area_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('area', [
            'id' => $this->primaryKey(),
            'areaname' => $this->string()->unique(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('area');
    }
}
