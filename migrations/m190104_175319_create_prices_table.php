<?php

use yii\db\Migration;

/**
 * Handles the creation of table `price`.
 */
class m190104_175319_create_prices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('price', [
            'id' => $this->primaryKey(),
            'id_area' => $this->integer()->notNull(),
            'price' => $this->integer()->unique(),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('price');
    }
}
