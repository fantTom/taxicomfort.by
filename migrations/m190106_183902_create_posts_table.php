<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m190106_183902_create_posts_table extends Migration
{
    /**
     * Должности
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'namepost' => $this->string()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('posts');
    }
}
