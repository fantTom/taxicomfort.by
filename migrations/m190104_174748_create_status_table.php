<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m190104_174748_create_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'statusname' => $this->string()->notNull()->unique(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('status');
    }
}
