<?php

use yii\db\Migration;

/**
 * Handles the creation of table `streets`.
 */
class m190106_191145_create_streets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('streets', [
            'id' => $this->primaryKey(),
            'locality' => $this->string(),
            'namestreet' => $this->string()->unique(),
            'type_id' => $this->integer()->notNull(), //улица, проспект, аллея, шоссе, бульвар, тупик, переулок, площадь
            'area_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('streets');
    }
}
