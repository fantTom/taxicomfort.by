<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employees`.
 */
class m190104_174816_create_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('employees', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'surname' => $this->string()->notNull()->unique(),
            'patronymic' => $this->string()->notNull()->unique(),
            'floor' => $this->smallInteger()->notNull()->defaultValue(0),
            'dateofbirth' => $this->integer()->notNull(),
            'post' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),


        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('employees');
    }
}
