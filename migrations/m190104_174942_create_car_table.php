<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car`.
 */
class m190104_174942_create_car_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car', [
            'id' => $this->primaryKey(),
            'brand' =>  $this->integer()->notNull(),
            'yearofissue' => $this->integer()->notNull(),
            'color' =>  $this->string()->notNull()->unique(),
            'bodytype' =>   $this->smallInteger()->notNull() ,
            'statenumber' => $this->string()->notNull()->unique(),
            'created_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('car');
    }
}
