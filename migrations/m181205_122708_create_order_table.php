<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m181205_122708_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'customername' => $this->string()->notNull(),
            'phonenumber' => $this->string(19)->notNull(),
            'street' => $this->string()->notNull(),
            'housenumber' => $this->integer()->notNull(),
            'hullnumber' => $this->string(),
            'entrancenumber' => $this->string(),
            'commentcustomer' => $this->text(),
            'status' => $this->integer(6)->notNull()->defaultValue(0),
            'created_at' => $this->string(19)->notNull(),
            'updated_at' => $this->string(19),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order');
    }
}
