<?php

use yii\db\Migration;

/**
 * Handles the creation of table `driver`.
 */
class m190104_175047_create_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('driver', [
            'id' => $this->primaryKey(),
            'id_employee' => $this->integer()->notNull(),
            'license' =>   $this->string()->unique(), //водительское удостоверение
            'experience' =>   $this->integer()->notNull(),  //водительский стаж
            'created_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('driver');
    }
}
