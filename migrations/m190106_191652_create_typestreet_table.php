<?php

use yii\db\Migration;

/**
 * Handles the creation of table `typestreet`.
 */
class m190106_191652_create_typestreet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('typestreet', [
            'id' => $this->primaryKey(),
            'nametype' => $this->string()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('typestreet');
    }
}
