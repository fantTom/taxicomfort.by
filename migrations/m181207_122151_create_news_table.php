<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m181207_122151_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(), // Название статьи
            'author_id' => $this->integer()->notNull(), //Автор
            'category_id' => $this->integer()->notNull(), //Номер категории
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('news');
    }
}
