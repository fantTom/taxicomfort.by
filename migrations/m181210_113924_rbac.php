<?php

use yii\db\Migration;

/**
 * Class m181210_113924_rbac
 */
class m181210_113924_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Yii::$app->runAction('migrate/up', [
            'migrationPath' => '@yii/rbac/migrations',
            'interactive' => false, // таким образом мы всегда говорим yes на все запросы в консоли
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181210_113924_rbac cannot be reverted.\n";

        Yii::$app->runAction('migrate/down', [
            'migrationPath' => '@yii/rbac/migrations',
            'interactive' => false,
        ]);
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181210_113924_rbac cannot be reverted.\n";

        return false;
    }
    */
}
