<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use yii\widgets\Breadcrumbs;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' =>'<p> ' . Html::img('@web/img/logo 58х39.png', ['alt'=>Yii::$app->name]) . Yii::$app->name .'</p> ',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-lg navbar-fixed-top',
            'style' => "background-color: #FFC100;"
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right b'],
        'items' => [
            ['label' => 'Новости', 'url' => ['/main/news/index']],
            ['label' => 'Отзывы', 'url' => ['/main/reviews/index']],
            ['label' => 'Связаться с нами', 'url' => ['/main/contact/index']],

             ['label' => 'Зарегистрироваться', 'url' => ['/user/default/signup'], 'visible' => Yii::$app->user->isGuest] ,
             ['label' => 'Профиль', 'url' => ['/user/profile/index'],'visible' => !Yii::$app->user->isGuest],
             ['label' => 'Диспетчер','url' => ['/operator/order/list'],'visible' => Yii::$app->user->can('operator') ],
             ['label' => 'Администратор','url' => ['/admin/default/index'],'visible' => Yii::$app->user->can('viewAdminPanel')] ,
            Yii::$app->user->isGuest ? ( // Если пользователь не авторизован
                ['label' => 'Войти', 'url' => ['/user/default/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/user/default/logout'], 'post')
                . Html::submitButton(
                    'Выйти (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::$app->name ?>, 2018 -  <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
